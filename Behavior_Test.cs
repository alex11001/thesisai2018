﻿//using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Thesis
{
    class Behavior_Test : Behavior
    {
        Transform LookAtObj;
        Transform MoveToObj;
        public override void Begin()
        {
            LookAtObj = Object.FindObjectOfType<ObjectToLook>().transform;
            MoveToObj = Object.FindObjectOfType<ObjectToMove>().transform;
        }

        public override void Process()
        {
            if( controller.CurrentOrder != null )
            {
                (controller.CurrentOrder as Order_Patrol).PatrolRoute = new Vector3[3];
                for( int i = 0; i < 3; i++ )
                    (controller.CurrentOrder as Order_Patrol).PatrolRoute[i] = new Vector3( Random.Range( -3, 3 ), 0, Random.Range( -3, 3 ) );
                controller.SetBehavior( typeof( Behavior_Patrol ) );
            }

            controller.LookAt( LookAtObj );
            controller.MoveTo( MoveToObj );

            controller.ShouldFire1 = true;
            if( controller.Body.EquipedWeapon.Clip.AmmoCount <= 0 )
                controller.ShouldReload = true;
        }
    }
}
