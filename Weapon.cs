﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Thesis
{
    public class Weapon : Entity, DangerEntity
    {
        protected Animator animator;

        [NonSerialized] public Body Owner;

        [NonSerialized] public Clip Clip;

        [NonSerialized] public Vector3 ViewPosition = new Vector3( 0.1f, -0.17f, 0.35f ); //temp
        [NonSerialized] public Quaternion ViewRotation = new Quaternion();

        [NonSerialized] public Transform ClipPosition;
        [NonSerialized] public Transform Sight;
        [NonSerialized] public Transform Barrel;
        [NonSerialized] public Transform EjectionPort;
        [NonSerialized] public Transform RightHand;
        [NonSerialized] public Transform LeftHand;

        [NonSerialized] public Vector3 EjectionDirection = new Vector3( -10, 0 );
        protected BulletType DefaultBulletType = BulletType.BT_Default;
        public BulletType BulletType { get { return (Clip == null ? DefaultBulletType : Clip.BulletType); } }
        
        [NonSerialized] public float fire_rate = 0.15f;
        [NonSerialized] public bool fire = false;

        [SerializeField] public float Range = 1000; //meters
        [SerializeField] public float DeviationHeight = 0.62f; //meters
        [SerializeField] public float DeviationSide = 0.82f; //meters

        protected float DeviationAngleHeight;
        protected float DeviationAngleSide;


        protected float NextFireTime = 0;

        protected virtual void Start()
        {
            animator = GetComponent<Animator>();

            Transform[] childs = GetComponentsInChildren<Transform>();
            for( int i = 0; i < childs.Length; i++ )
            {
                Transform child = childs[i];
                if( child.name == "Sight" )
                    Sight = child;
                else if( child.name == "Barrel" )
                    Barrel = child;
                else if( child.name == "RightHand" )
                    RightHand = child;
                else if( child.name == "LeftHand" )
                    LeftHand = child;
                else if( child.name == "ClipPosition" )
                    ClipPosition = child;
                else if( child.name == "EjectionPort" )
                    EjectionPort = child;
            }

            //temp
            Clip = AmmoManager.SpawnClip( ClipPosition.position, ClipPosition.rotation, "DefaultClip", ClipPosition );
            Clip.Pickup();
            DefaultBulletType = Clip.BulletType;

            RecalcDeviations();
        }

        protected virtual void Update()
        {
            if( fire )
            {
                if( NextFireTime < Time.time )
                {
                    Shoot();
                    NextFireTime = Time.time + fire_rate;
                }
            }
        }

        protected virtual void Shoot()
        {
            if( Clip.AmmoCount > 0 )
            {
                animator.SetTrigger( "Shoot" );
                if( Owner != null )
                Owner.animator.SetTrigger( "Shoot" );

                Bullet bullet = Clip.DequeueBullet();
                bullet.CurrentPosition = Sight.position;
                bullet.Gun = this;
                bullet.Shooter = Owner;

                Shell shell = AmmoManager.ShootBullet( Barrel.position,
                    Barrel.rotation * Quaternion.Euler( DeviationAngleHeight * UnityEngine.Random.Range( -1f, 1f ), DeviationAngleSide * UnityEngine.Random.Range( -1f, 1f ), 0 ),
                    bullet );

                AmmoManager.EjectBrass( EjectionPort.position, EjectionPort.rotation, EjectionDirection, shell );
            }
        }

        public virtual void Pickup( Body owner )
        {
            if( Owner != null )
                Owner.DropWeapon( this );

            Owner = owner;
        }

        public virtual void Drop( bool calledByOwner = false )
        {
            if( calledByOwner )
            {
                
            }
            else 
            {
                if( Owner != null )
                    Owner.DropWeapon( this );
            }
        }

        
        public virtual void Equip( bool calledByOwner = false )
        {
            if( calledByOwner )
            {
                transform.parent = Owner.WeaponPosition;
                SetupPosition();
            }
            else 
            {
                if( Owner != null )
                    Owner.EquipWeapon( this );
            }  
        }

        public virtual void Holster( bool calledByOwner = false )
        {
            if( calledByOwner )
            { }
            else
            {
                if( Owner != null )
                    Owner.HolsterWeapon( this );
            }
        }

        public virtual void Reload( Clip newclip = null )
        {
            if( newclip != null )
            {
                Clip.Drop();
                Clip = newclip;
                Clip.Pickup( ClipPosition );
                Clip.transform.localPosition = Vector3.zero;
                Clip.transform.localRotation = Quaternion.identity;
                Clip.transform.localScale = Vector3.one;

                DefaultBulletType = Clip.BulletType;
            }
            else
            {
                Owner.ReloadWeapon();
            }
        }

        protected virtual void SetupPosition()
        {
            //transform.position = Owner.WeaponPosition.position;
            //transform.rotation = Owner.WeaponPosition.rotation;

            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;

            Owner.WeaponPosition.localPosition = ViewPosition;
            Owner.WeaponPosition.localRotation = ViewRotation;
        }

        public virtual void RecalcDeviations()
        {
            DeviationAngleHeight = Mathf.Atan( DeviationHeight / Range ) * Mathf.Rad2Deg;
            DeviationAngleSide = Mathf.Atan( DeviationSide / Range ) * Mathf.Rad2Deg;
        }

        public virtual float GetDanger() { return GetDangerReal(); }
        public virtual float GetDangerReal()
        {
            return AmmoManager.CalcDanger( BulletType ) * Range * (Clip == null ? 1 : (Clip.AmmoCount/Clip.MaxAmmoCount));
        }

        public virtual float GetDangerPotential()
        {
            return AmmoManager.CalcDanger( DefaultBulletType ) * Range * (Clip == null ? 1 : (Clip.MaxAmmoCount));
        }
    }
}
