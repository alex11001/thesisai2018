﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Thesis
{
    public class BodyControllerStateInfo
    {
        protected BodyController Controller;
        public BodyControllerStateInfo( BodyController controller ) { Controller = controller; }

        public float Heat { get; set; } //how much pressure 
        public bool IsInCombat { get; set; } //fighting
        public bool IsShouldPatrol { get; set; } 
        public bool IsAlert { get; set; } //enemy around or something wrong
    }
}
