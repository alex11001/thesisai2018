﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Thesis
{
    public class Formation
    {
        public class PosDir
        {
            public PosDir( Vector3 pos = new Vector3(), Vector3 dir = new Vector3() ) { Position = pos; Direction = dir; }
            public PosDir( float px = 0, float py = 0, float pz = 0, float dx = 0, float dy = 0, float dz = 0 ) : this( new Vector3( px, py, pz ), new Vector3( dx, dy, dz ) ) { }

            public Vector3 Position;
            public Vector3 Direction;

            public static explicit operator PosDir( Vector3 v ) { return new PosDir(v); }
            //public static explicit operator PosDir[]( Vector3[] v ) { return new PosDir( v ); }
        }

        public Formation( float distMin = 1, float distMax = 5, Vector3[] asides = null, Vector3[] nextLeader = null )
        {
            DistanceMin = distMin; DistanceMax = distMax; Asides = Array.ConvertAll( asides, p => (PosDir)p); NextLeader = Array.ConvertAll( nextLeader, p => (PosDir)p );
        }

        public Formation( float distMin = 1, float distMax = 5, PosDir[] asides = null, PosDir[] nextLeader = null )
        {
            DistanceMin = distMin; DistanceMax = distMax; Asides = asides; NextLeader = nextLeader;
        }

        public float DistanceMin;
        public float DistanceMax;

        public PosDir[] Asides;
        public PosDir[] NextLeader;


        public static Formation Kare { get { return new Formation( asides: new Vector3[] { new Vector3( -0.5f, 0, 0 ), new Vector3( 0.5f, 0, 0 ), new Vector3( -0.5f, 0, -1 ), new Vector3( 0.5f, 0, -1 ) }, nextLeader: new Vector3[] { new Vector3( 0, 0, -1 ) } ); } }
        public static Formation Phalanx { get { return new Formation( 0.5f, 1, nextLeader: new Vector3[] { new Vector3( -1, 0, 0 ), new Vector3( 1, 0, 0 ), new Vector3( 0, 0, -1 ) } ); } }
        public static Formation Svinfylking { get { return new Formation( 1, 3, new Vector3[] { new Vector3( 0, 0, -1 ) }, new Vector3[] { new Vector3( -1, 0, -1 ), new Vector3( 1, 0, -1 ) } ); } }
        public static Formation Echelon { get { return new Formation( 2, 7, nextLeader: new Vector3[] { new Vector3( -1, 0, -1 ) } ); } }
        public static Formation Column { get { return new Formation( nextLeader: new Vector3[] { new Vector3( 0, 0, -1 ) } ); } }
        public static Formation Triangle { get { return new Formation( 0.5f, 2, new PosDir[] { new PosDir( -1, 0, -1, -1, 0, 0 ), new PosDir( 1, 0, -1, 1, 0, 0 ) } ); } }
        public static Formation Diamond { get { return new Formation( 0.5f, 2, new PosDir[] { new PosDir( -1, 0, -1, -1, 0, 0 ), new PosDir( 1, 0, -1, 1, 0, 0 ), new PosDir( 0, 0, -2, 0, 0, -1 ) } ); } }
    }
}
