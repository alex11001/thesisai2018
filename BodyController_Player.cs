﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Thesis
{
    public class BodyController_Player : BodyController
    {
        public float Sensivity = 1;
        public float VerticalMin = -80;
        public float VerticalMax = 90;

        public override void Init( Body body, bool force_reinit = false )
        {
            bool isinit = IsInitilized;
            base.Init( body, force_reinit );

            StateInfo = new BodyControllerStateInfo( this );

            if( !isinit || force_reinit )
                Start();
        }

        public override void Process()
        {
            base.Process();

            Body.input_move.x = Input.GetAxis( "Horizontal" );
            Body.input_move.z = Input.GetAxis( "Vertical" );

            Body.sprint = Input.GetAxis( "Sprint" );
            Body.walk = Input.GetAxis( "Walk" );

            Body.crouch = Input.GetAxis( "Crouch" );
            Body.jump = Input.GetButtonDown( "Jump" );

            Body.lean = Input.GetAxis( "Lean" );

            Body.roll_user = Input.GetButton( "Roll" );

            Body.fire1 = Input.GetAxis( "Fire1" );
            Body.fire2 = Input.GetAxis( "Fire2" );
            Body.fire3 = Input.GetAxis( "Fire3" );
        }


        public override void Update()
        {
            base.Update();

            UpdateRotation();
        }

        public override void LateUpdate()
        {
            base.LateUpdate();

            Body.lean = Input.GetAxis( "Lean" );
        }

        float CurrentHorizontalRotation;
        float CurrentVerticalRotation;
        protected virtual void UpdateRotation()
        {
            CurrentHorizontalRotation += Input.GetAxis( "Mouse X" ) * Sensivity;
            CurrentVerticalRotation += Input.GetAxis( "Mouse Y" ) * Sensivity;

            CurrentVerticalRotation = Mathf.Clamp( CurrentVerticalRotation, VerticalMin, VerticalMax );

            Body.input_horizontal_rotation = Quaternion.AngleAxis( CurrentHorizontalRotation, Vector3.up );
            Body.input_vertical_rotation = Quaternion.AngleAxis( -CurrentVerticalRotation, Vector3.right );
        }
    }
}
