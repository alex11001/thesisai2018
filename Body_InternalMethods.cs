﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Thesis
{
    public partial class Body : EntitySided, DamageReceiver
    {
        public enum UpdateTypeEnum { Fixed, Framed }

        protected Transform RightHand;
        protected Transform LeftHand;
        protected Transform RightFoot;
        protected Transform LeftFoot;

        public Transform Head { get; protected set; }
        protected Transform Neck;
        protected Transform Spine;

        [NonSerialized]
        public Transform WeaponPosition;

        PhysicMaterial PhyMaterial;

        //parameters
        protected int idX;
        protected int idY;
        protected int idCrouch;
        protected int idAscending;
        protected int idTurning;
        protected int idJumpLeg;
        protected int idPhyX;
        protected int idPhyY;
        protected int idPhyZ;
        protected int idGrounded;
        protected int idProne;
        protected int idSlide;
        protected int idRoll;
        protected int idRollTrigger;
        protected int idHandState;
        protected int idShot;
        protected int idMoveLadder;
        protected int idOnLadder;

        //states
        protected int idStand;
        protected int idJump;
        //protected int idRoll; 

        Vector3 GroundCheckerStraight = new Vector3();
        Vector3 GroundCheckerSide = new Vector3();

        void GetComponents()
        {
            animator = GetComponent<Animator>();
            rigidbody = GetComponent<Rigidbody>();
            collider = GetComponent<Collider>();

            RightHand = animator.GetBoneTransform( HumanBodyBones.RightHand );
            LeftHand = animator.GetBoneTransform( HumanBodyBones.LeftHand );
            RightFoot = animator.GetBoneTransform( HumanBodyBones.RightFoot );
            LeftFoot = animator.GetBoneTransform( HumanBodyBones.LeftFoot );

            Head = animator.GetBoneTransform( HumanBodyBones.Head );
            Neck = animator.GetBoneTransform( HumanBodyBones.Neck );
            Spine = animator.GetBoneTransform( HumanBodyBones.Spine );

            if( Neck == null ) Neck = Head;

            PhyMaterial = collider.material;

            idX = Animator.StringToHash( "X" );
            idY = Animator.StringToHash( "Y" );
            idCrouch = Animator.StringToHash( "Crouch" );
            idAscending = Animator.StringToHash( "Ascending" );
            idTurning = Animator.StringToHash( "Turning" );
            idJumpLeg = Animator.StringToHash( "JumpLeg" );
            idPhyX = Animator.StringToHash( "PhyX" );
            idPhyY = Animator.StringToHash( "PhyY" );
            idPhyZ = Animator.StringToHash( "PhyZ" );
            idGrounded = Animator.StringToHash( "Grounded" );
            idProne = Animator.StringToHash( "Prone" );
            idSlide = Animator.StringToHash( "Slide" );
            idRoll = Animator.StringToHash( "Roll" );
            idRollTrigger = Animator.StringToHash( "RollTrigger" );
            idHandState = Animator.StringToHash( "HandState" );
            idShot = Animator.StringToHash( "Shot" );
            idMoveLadder = Animator.StringToHash( "MoveLadder" );
            idOnLadder = Animator.StringToHash( "OnLadder" );

            idStand = Animator.StringToHash( "Stand" );
            idJump = Animator.StringToHash( "Jump" );

            
            GroundCheckerStraight.z = collider.bounds.extents.z;
            GroundCheckerSide.x = collider.bounds.extents.x;

            WeaponPosition = new GameObject( "WeaponPosition" ).transform;
            WeaponPosition.parent = Head;
            WeaponPosition.localPosition = Vector3.zero;
            WeaponPosition.localRotation = Quaternion.identity;
        }

        protected override void Awake()
        {
            base.Awake();
            EnvironmentManager.Agents.Add( this );
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            EnvironmentManager.Agents.Remove( this );
        }

        void HandleLateUpdateType()
        {
            if( update_type )
                StopCoroutine( LateFixedUpdateCycle() );
            else
                StartCoroutine( LateFixedUpdateCycle() );
        }

        IEnumerator LateFixedUpdateCycle()
        {
            while( !update_type )
            {
                yield return new WaitForFixedUpdate();
                LateFixedUpdate();
            }
        }
    }
}