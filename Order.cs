﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Thesis
{
    public abstract class Order
    {
        public Order( Order nextorder = null ) { NextOrder = nextorder; }

        protected Order nextorder;
        public virtual Order NextOrder
        {
            get { return (nextorder != null) ? nextorder : NextOrderDefault; }
            set { nextorder = value; }
        }

        public virtual Order NextOrderDefault { get { return new Order_Default(); } }
        public virtual bool IsOrderSucceed( BodyController_AI controller ) { return false; }
    }
}
