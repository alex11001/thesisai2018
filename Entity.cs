﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Thesis
{
    public class Entity : MonoBehaviour
    {
        [NonSerialized] public new Collider collider;
        [NonSerialized] public new Rigidbody rigidbody;

        protected virtual void Awake()
        {
            EnvironmentManager.Entities.Add( this );
        }

        protected virtual void OnDestroy()
        {
            EnvironmentManager.Entities.Remove( this );
        }
    }

    public interface DangerEntity
    {
        float GetDanger();
        float GetDangerReal();
        float GetDangerPotential();
    }
}
