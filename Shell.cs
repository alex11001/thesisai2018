﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Thesis
{
    public class Shell : MonoBehaviour
    {
        public new Collider collider;
        public new Rigidbody rigidbody;

        protected virtual void Awake()
        {
            collider = GetComponent<Collider>();
            rigidbody = GetComponent<Rigidbody>();

            enabled = false;
        }
    }
}
