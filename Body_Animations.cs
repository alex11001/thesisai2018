﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Thesis
{
    public partial class Body : EntitySided, DamageReceiver
    {
        [NonSerialized] public float IKRightHand, IKLeftHand;
        [NonSerialized] public float IKRightLeg,  IKLeftLeg;

        protected virtual void OnAnimatorIK( int layerIndex )
        {
            Neck.rotation = transform.rotation * input_vertical_rotation;

            switch( layerIndex )
            {
                case 0:
                    OnAnimatorIKLayer0();
                    break;
                case 1:
                    OnAnimatorIKLayer1();
                    break;
                default: break;
            }
        }

        protected virtual void OnAnimatorIKLayer0() { }

        protected virtual void OnAnimatorIKLayer1()
        {
            Weapon weapon = Inventory.EquipedWeapon;
            if( weapon != null )
            {
                animator.SetIKPosition( AvatarIKGoal.RightHand, weapon.RightHand.position );
                animator.SetIKPositionWeight( AvatarIKGoal.RightHand, IKRightHand );

                animator.SetIKPosition( AvatarIKGoal.LeftHand, weapon.LeftHand.position );
                animator.SetIKPositionWeight( AvatarIKGoal.LeftHand, IKLeftHand );

                animator.SetIKRotation( AvatarIKGoal.RightHand, weapon.RightHand.rotation );
                animator.SetIKRotationWeight( AvatarIKGoal.RightHand, IKRightHand );

                animator.SetIKRotation( AvatarIKGoal.LeftHand, weapon.LeftHand.rotation );
                animator.SetIKRotationWeight( AvatarIKGoal.LeftHand, IKLeftHand );
            }
        }
    }
}
