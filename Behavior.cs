﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Thesis
{
    public abstract class Behavior
    {
        protected Body body;
        protected BodyController_AI controller;

        public virtual Body Body { get { return body; } set { body = value; } }
        public BodyController_AI Controller { get { return controller; } set { controller = value; } }

        public virtual List<MemoryBlock> Memory { get { return controller.Memory; } set { controller.Memory = value; } }

        public Behavior( Body body = null, BodyController_AI controller = null ) { this.body = body; this.controller = controller; Begin();  }

        public abstract void Begin();
        public abstract void Process();

        public virtual void SwitchBehavior( Behavior new_behavior ) { controller.CurrentBehavior = new_behavior; }
        public  void SwitchBehaviorDefault( Behavior new_behavior ) { controller.CurrentBehavior = new_behavior; }

        /// <summary> returns number [0,1] represents how effective this behavior will be in the current state of cotroller </summary>
        public static float Priority( BodyController controller ) { return 0; }

        public static Type GetBehaviorFitOrder( Order order )
        {
            string order_type = order.GetType().ToString();
            switch( order_type )
            {
                case "Assets.Thesis.Order_Patrol": return typeof( Behavior_Patrol );
                default: return typeof(Behavior_Default);
            }
        }
    }
}
