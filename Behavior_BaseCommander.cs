﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Thesis
{
    public interface Commander
    {
        Body Body { get; set; }
        List<Squad> UnderCommand { get; }

        void TakeCommand( Squad squad );
        void CeaseCommand( Squad squad );
    }

    public static class CommanderExntends
    {
        public static void TakeCommandDefault( this Commander commander, Squad squad ) { if( commander.UnderCommand.Contains( squad ) ) return; commander.UnderCommand.Add( squad ); }
        public static void CeaseCommandDefault( this Commander commander, Squad squad ) { commander.UnderCommand.Remove( squad ); }
    }
    
    public class Behavior_BaseCommander : Behavior/*_BaseCombatSquad*/, Commander
    {
        public List<Squad> UnderCommand { get; protected set; }
        protected Behavior BaseBehavior;

        public Behavior_BaseCommander( Body body, BodyController_AI controller ) : base( body, controller )
        {
            UnderCommand = new List<Squad>();
            BaseBehavior = controller.CurrentBehavior;
        }

        public override void SwitchBehavior( Behavior new_behavior )
        {
            BaseBehavior = new_behavior;
        }

        public override void Begin() { }
        public override void Process()
        {
            //do commander stuff

            BaseBehavior.Process();
        }

        public virtual void TakeCommand( Squad squad ) { this.TakeCommandDefault( squad ); }
        public virtual void CeaseCommand( Squad squad ) { this.CeaseCommandDefault( squad ); }

        public virtual Squad FormSquad( Body[] newsquadunits = null, bool putselfin = true )
        {
            List<Body> squadunits;
            if( newsquadunits != null )
            {
                squadunits = newsquadunits.ToList();
                if( putselfin ) squadunits.Insert( 0, body );
            }
            else
            {
                squadunits = new List<Body>();
                if( putselfin ) //BaseBehavior is Behavior_BaseCombatSquad )
                    squadunits.Add( body );

                IEnumerable<Body> newunits = EnvironmentManager.Agents.Where( x =>
                (x != body) &&
                (x.Side == body.Side) &&
                (x.CurrentController is BodyController_AI) &&
                (!((x.CurrentController as BodyController_AI).CurrentBehavior is Behavior_BaseCombatSquad) ||
                (/*((x.CurrentController as BodyController_AI).CurrentBehavior is Behavior_BaseCombatSquad) &&*/
                ((x.CurrentController as BodyController_AI).CurrentBehavior as Behavior_BaseCombatSquad).CurrentSquad == null)) );
                if( newunits != null )
                    squadunits.AddRange( newunits.OrderBy( x => Vector3.Distance( x.transform.position, body.transform.position ) ).Take( 3 ) );
            }
            Squad squad = new Squad();
            squad.Units = squadunits.ToArray();
            squad.CurrentCommander = this;
            squad.UpdateSquad();
            TakeCommand( squad );
            return squad;
        }

        public virtual void DisbandSquad( Squad squad )
        {
            if( !UnderCommand.Contains( squad ) )
                return;

            squad.DisbandSquad();
        }

        public virtual void DisposeFormation( Formation formation, Behavior_BaseCombatSquad[] units )
        {
            if( units == null )
                return;

            float distance = UnityEngine.Random.Range( formation.DistanceMin, formation.DistanceMax );

            List<Behavior_BaseCombatSquad> leaders = new List<Behavior_BaseCombatSquad> { units[0] };
            Behavior_BaseCombatSquad leader;
            
            int j = 1, k = 0;
            while( true )
            {
                leader = leaders[k++];
                if( formation.Asides != null )
                {
                    for( int i = 0; i < formation.Asides.Length && j < units.Length; i++, j++ )
                    {
                        Transform form_pos = new GameObject( "FormationBlock" ).transform;
                        form_pos.position = leader.Body.transform.position + leader.Body.transform.rotation * (formation.Asides[i].Position * distance);
                        form_pos.parent = leader.Body.transform;
                        units[j].FormationTarget = form_pos;
                    }
                }

                if( formation.NextLeader != null )
                {
                    for( int i = 0; i < formation.NextLeader.Length && j < units.Length; i++, j++ )
                    {
                        Transform form_pos = new GameObject( "FormationBlock" ).transform;
                        form_pos.position = leader.Body.transform.position + leader.Body.transform.rotation * (formation.NextLeader[i].Position * distance);
                        form_pos.parent = leader.Body.transform;
                        units[j].FormationTarget = form_pos;
                        leaders.Add( units[j] );
                    }
                }

                if( j >= units.Length || k >= leaders.Count )
                    break;
            }
        }

        public virtual void DisbandFormation( Behavior_BaseCombatSquad[] units )
        {
            for( int i = 0; i < units.Length; i++ )
            {
                Transform unit = units[i].Body.transform;
                foreach( Transform t in unit )
                    if( t.name == "FormationBlock" )
                        UnityEngine.Object.Destroy( t );

                units[i].FormationTarget = null;
            }
        }

        //public static implicit operator Behavior_BaseCombatSquad( Behavior_BaseCommander comander ) { return (comander.BaseBehavior as Behavior_BaseCombatSquad); }
        public static explicit operator Behavior_BaseCombatSquad( Behavior_BaseCommander comander ) { return (comander.BaseBehavior as Behavior_BaseCombatSquad); }
    }
}
