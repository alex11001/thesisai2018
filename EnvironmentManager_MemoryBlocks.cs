﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Thesis
{
    public partial class EnvironmentManager : MonoBehaviour
    {
        public MemoryBlock GetLastPositionBlock( Body target )
        {
            int index = Agents.IndexOf( target );
            if( index == -1 )
                return null;

            Vector3[] positions = AgentLastPosition[index].ToArray();

            MemoryBlock_AgentLastPosition alp = new MemoryBlock_AgentLastPosition();
            alp.Agent = target;
            if( positions.Length > 1 )
                alp.LastSeenPosition = positions[1];
            if( positions.Length > 2 )
                alp.LastSeenPosition2 = positions[2];

            return alp;
        }
    }
}
