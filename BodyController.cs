﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Thesis
{
    public class BodyController
    {
        protected bool IsInitilized = false;

        public virtual Body Body { get; set; }

        public BodyControllerStateInfo StateInfo;

        public BodyController( Body body = null, bool force_reinit = false ) { Init( body, force_reinit ); }
        public virtual void Init( Body body, bool force_reinit = false  ) { Body = body; IsInitilized = true; }
        public virtual void Process() { }

        public virtual void Awake() { }
        public virtual void Start() { }
        public virtual void Update() { }
        public virtual void FixedUpdate() { }
        public virtual void LateUpdate() { }
    }
}
