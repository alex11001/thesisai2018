﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Thesis
{
    public class Behavior_BaseCombat : Behavior
    {
        public virtual Body CurrentEnemy { get; set; }

        public Behavior_BaseCombat( Body body, BodyController_AI controller ) : base( body, controller ) { }

        public override void Begin() { }
        public override void Process() { }

        protected virtual Transform GetTarget()
        {


            return new GameObject().transform;
        }
    }
}
