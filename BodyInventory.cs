﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Thesis
{
    public class BodyInventory : List<Weapon>
    {
        protected Body Owner;
        public BodyInventory( Body owner ) { Owner = owner; }

        int EquipedIndex = 0;
        public Weapon EquipedWeapon
        {
            get { return (Count > 0) ? this[EquipedIndex] : null; }
            set { Equip( value ); }
        }

        public new virtual int Add( Weapon weapon )
        {
            if( weapon == null )
                return -1;

            if( IndexOf( weapon ) >= 0 )
                return IndexOf( weapon );

            base.Add( weapon );
            weapon.Pickup( Owner );

            return Count - 1;
        }

        public virtual void Drop( Weapon weapon ) { Remove( weapon ); }
        public new virtual void Remove( Weapon weapon )
        {
            if( !Contains( weapon ) )
                return;

            base.Remove( weapon );

            weapon.Drop( true );
        }

        public virtual void Equip( Weapon weapon )
        {
            if( Count > 0 && EquipedIndex >= 0 )
                Owner.HolsterWeapon( this[EquipedIndex] );

            int index = IndexOf( weapon );
            if( index > 0 )
                EquipedIndex = index;
            else
                EquipedIndex = Add( weapon );

            weapon.Equip( true );
        }

        public virtual void Holster( Weapon weapon )
        {
            if( !Contains( weapon ) )
                return;

            EquipedIndex = -1;
            weapon.Holster( true );
        }
    }
}
