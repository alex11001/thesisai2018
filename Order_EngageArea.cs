﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Thesis
{
    public class Order_EngageArea : Order
    {
        public Order_EngageArea( Vector3[] area = null, Order nextorder = null ) : base(nextorder) { Area = area; }

        protected Vector3 Min, Max;
        protected Vector3[] area;
        public Vector3[] Area { get { return area; } set { area = value; RecalcMinMax(); } }

        public override Order NextOrderDefault { get { return new Order_ProtectArea( Area ); } }
        public override bool IsOrderSucceed( BodyController_AI controller )
        {
            if( Area != null )
            {
                Body[] enemies = EnvironmentManager.Agents.Where( x => x.Side != controller.Body.Side && x.IsAlive ).ToArray();
                for( int i = 0; i < enemies.Length; i++ )
                {
                    Body enemy = enemies[i];
                    if( (Min.x <= enemy.transform.position.x && enemy.transform.position.x <= Max.x) || (Max.x <= enemy.transform.position.x && enemy.transform.position.x <= Min.x) &&
                        //(Min.y <= enemy.transform.position.y && enemy.transform.position.y <= Max.y) || (Max.y <= enemy.transform.position.y && enemy.transform.position.y <= Min.y) &&
                        (Min.z <= enemy.transform.position.z && enemy.transform.position.z <= Max.z) || (Max.z <= enemy.transform.position.z && enemy.transform.position.z <= Min.z) )
                    {
                        return false;
                    }
                }
                return true;
            }
            
            return false;
        }

        protected void RecalcMinMax()
        {
            if( area == null )
                return;

            Min = new Vector3( float.MaxValue, float.MaxValue, float.MaxValue );
            Max = new Vector3( float.MinValue, float.MinValue, float.MinValue );
            for( int i = 0; i < Area.Length; i++ )
            {
                Vector3 point = Area[i];
                if( point.x < Min.x ) Min.x = point.x;
                //if( point.y < Min.y ) Min.y = point.y;
                if( point.z < Min.z ) Min.z = point.z;
                if( point.x > Max.x ) Max.x = point.x;
                //if( point.y > Max.y ) Max.y = point.y;
                if( point.z > Max.z ) Max.z = point.z;
            }
        }
    }
}
