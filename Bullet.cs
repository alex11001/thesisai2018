﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Thesis
{
    [DefaultExecutionOrder( -48 )]
    public class Bullet : Entity, DangerEntity
    {
        [NonSerialized] public Vector3 CurrentPosition;
        [SerializeField] public BulletType type = BulletType.BT_Default;
        [SerializeField] public float Speed = 910;
        [SerializeField] public float Damage = 4;
        [SerializeField] public float Energy = 15;

        [NonSerialized] public Body Shooter;
        [NonSerialized] public Weapon Gun;
        [NonSerialized] public Shell Shell;

        protected TrailRenderer trail;
        protected RaycastHit hit;

        protected override void Awake()
        {
            base.Awake();

            trail = GetComponent<TrailRenderer>();
            enabled = enabled;
        }

        protected virtual void Start()
        {
            EnvironmentManager.Add( this );
            Destroy( gameObject, 5 );
        }

        protected virtual void FixedUpdate()
        {
            CurrentPosition += (transform.forward * Speed * Time.fixedDeltaTime);

            Physics.Raycast( transform.position, transform.forward, out hit, Vector3.Distance( transform.position, CurrentPosition ), AmmoManager.ignoreBodyLayerMask );

            transform.position = CurrentPosition;

            if( hit.transform != null && !hit.collider.isTrigger )
            {
                AmmoManager.DrawDecal( hit, type );

                DamageReceiver Combatant = hit.transform.GetComponent<DamageReceiver>();
                if( Combatant != null )
                    Combatant.ReceiveDamage( new HitInfo( Shooter, DamageAmount: Damage ) );

                if( hit.rigidbody != null )
                    hit.rigidbody.AddForceAtPosition( transform.forward * Energy, hit.normal + hit.point, ForceMode.Impulse ); //hit.rigidbody.AddForceAtPosition( -hit.normal * Energy, -hit.normal, ForceMode.Impulse );

                Destroy( gameObject );
            }
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            EnvironmentManager.Remove( this );
        }

        public new bool enabled
        {
            get
            {
                return base.enabled;
            }
            set
            {
                base.enabled = value;
                if( trail != null )
                    trail.enabled = value;
            }
        }

        public virtual float GetDangerReal() { return GetDanger(); }
        public virtual float GetDangerPotential() { return GetDanger(); }
        public virtual float GetDanger()
        {
            return Damage + Energy * 0.5f + Speed * 0.1f;
        }
    }
}
