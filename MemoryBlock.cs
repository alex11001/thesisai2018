﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Thesis
{
    public abstract class MemoryBlock
    {
    }

    public class MemoryBlock_Agent : MemoryBlock
    {
        public Body Agent;
        public int HitsCount; //seen
        public int ShootsCount; //seen
        public float Heat { get { return Agent.CurrentController.StateInfo.Heat; } }


        public Vector3 LastSeenPosition;
        public Vector3 LastSeenPosition2; //second step behind
    }

    public class MemoryBlock_AgentLastPosition : MemoryBlock
    {
        public Body Agent;
        public Vector3 LastSeenPosition;
        public Vector3 LastSeenPosition2; //second step behind
    }
}
