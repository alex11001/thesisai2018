﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Thesis
{
    public partial class Body : EntitySided, DamageReceiver
    {
        protected BodyController currentcontroller { get; private set; }

        public BodyController CurrentController
        { get { return GetBodyController(); } set { SetBodyController( value ); } }
        public BodyController PrevController { get; private set; }

        public BodyController GetBodyController() { return currentcontroller; }
        public void SetBodyController( BodyController bodycontroller, bool saveprev = true )
        {
            //PrevController = currentcontroller;
            //if( saveprev ) currentcontroller = bodycontroller;
            //if( currentcontroller.Controlled == null ) currentcontroller.Controlled = this;
            if( saveprev ) PrevController = currentcontroller;
            currentcontroller = bodycontroller;
            currentcontroller.Body = this;
        }
    }
}
