﻿using System.Collections.Generic;

namespace Assets.Thesis
{
    public class FixedQueue<T> : Queue<T>
    {
        protected T[] array;
        public FixedQueue( int maxsize = 5 ) : base() { MaxCount = maxsize; array = new T[0]; }
        public FixedQueue( IEnumerable<T> collection, int maxsize = 5 ) : base( collection ) { MaxCount = maxsize; array = ToArray(); }

        public int MaxCount { get; set; }

        public new void Enqueue( T item )
        {
            base.Enqueue( item );
            if( Count > MaxCount )
                Dequeue();
        }

        public new T Dequeue()
        {
            T obj = base.Dequeue();
            array = ToArray();
            return obj;
        }

        public new void Clear()
        {
            array = new T[0];
            base.Clear();
        }

        public T this[int key]
        {
            get { return array[key]; }
            set { array = ToArray(); }
        }
    }
}