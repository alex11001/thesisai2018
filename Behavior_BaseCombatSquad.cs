﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Thesis
{
    public class Squad : Behavior_BaseCombat
    {
        public override Body Body { get { return CurrentCommander.Body; } set { UnityEngine.Debug.LogError( "Attemp to assign squad body" ); } }

        protected Behavior_BaseCommander commander;
        public Behavior_BaseCommander CurrentCommander
        {
            get { return commander; }
            set { if(commander!=null) commander.CeaseCommand( this ); commander = value; if(commander!=null) commander.TakeCommand( this ); }
        }

        public Body[] Units;
        public override List<MemoryBlock> Memory { get; set; }

        public Squad( Body body = null, BodyController_AI controller = null ) : base( body, controller ) {  }

        public void UpdateSquad()
        {
            if( Units != null )
            {
                List<Body> units = Units.ToList();
                for( int i = 0; i < units.Count; i++ )
                {
                    BodyController_AI control = units[i].CurrentController as BodyController_AI;
                    if( control == null || !control.Body.IsAlive )
                    {
                        units.RemoveAt( i-- );
                        continue;
                    }

                    Behavior_BaseCombatSquad squaded = control.CurrentBehavior as Behavior_BaseCombatSquad;
                    if( squaded == null )
                    {
                        control.SetBehavior( typeof( Behavior_BaseCombatSquad ) );
                        squaded = control.CurrentBehavior as Behavior_BaseCombatSquad;
                        squaded.CurrentSquad = this;
                    }
                }
                Units = units.ToArray();
            }
        }

        public void DisbandSquad()
        {
            if( Units != null )
            {
                for( int i = 0; i < Units.Length; i++ )
                {
                    try { ((Units[i].CurrentController as BodyController_AI).CurrentBehavior as Behavior_BaseCombatSquad).CurrentSquad = null; }
                    catch( Exception e ) { UnityEngine.Debug.LogError( e ); }
                }
            }
            CurrentCommander = null;
        }
    }

    public class Behavior_BaseCombatSquad : Behavior_BaseCombat
    {
        public Behavior_BaseCombatSquad( Body body, BodyController_AI controller ) : base( body, controller ) { }

        public Squad CurrentSquad;
        public override Body CurrentEnemy
        {
            get { return (CurrentSquad == null) ? base.CurrentEnemy : CurrentSquad.CurrentEnemy; }
            set { if( CurrentSquad == null ) base.CurrentEnemy = value; else CurrentSquad.CurrentEnemy = value; }
        }
        public override List<MemoryBlock> Memory
        {
            get { return (CurrentSquad == null) ? base.Memory : CurrentSquad.Memory; }
            set { if( CurrentSquad == null ) base.Memory = value; else CurrentSquad.Memory = value; }
        }

        public bool IsInFormation { get { return FormationTarget != null; } }
        public UnityEngine.Transform FormationTarget;
        public UnityEngine.Vector3 FormationDirection;

        public override void Process()
        {
            base.Process();

            if( !CurrentSquad.CurrentCommander.Body.IsAlive )
            {
                if( CurrentSquad.Units.Length <= 2 )
                    CurrentSquad.DisbandSquad();

                controller.SetBehavior( typeof( Behavior_BaseCommander ) );
                CurrentSquad.CurrentCommander = controller.CurrentBehavior as Behavior_BaseCommander;
                CurrentSquad.UpdateSquad();
            }

            if( IsInFormation )
            {
                controller.MoveTo( FormationTarget );
                if( controller.StateInfo.IsAlert && !controller.StateInfo.IsInCombat )
                    controller.LookAtDir( FormationTarget.rotation * FormationDirection );
            }
        }
    }
}