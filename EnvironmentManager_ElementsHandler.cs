﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Thesis
{
    public partial class EnvironmentManager : MonoBehaviour
    {
        public static List<Body> Agents { get; protected set; }
        public static List<EntitySided> Sideds { get; protected set; }
        public static List<Entity> Entities { get; protected set; }
        public static List<Bullet> Bullets { get; protected set; }
        public static List<Queue<Vector3>> AgentLastPosition { get; protected set; }
        public static List<HitInfo> RegistredHits { get; protected set; }


        public static void Add<T>( T item )
        {
            switch( typeof( T ).ToString() )
            {
                case "Assets.Thesis.Bullet":
                    Bullets.Add( item as Bullet );
                    break;
                case "Assets.Thesis.Body":
                    Agents.Add( item as Body );
                    AgentLastPosition.Add( new Queue<Vector3>( 3 ) );
                    break;
                default: return;
            }
        }

        public static void Remove<T>( T item )
        {
            switch( typeof( T ).ToString() )
            {
                case "Assets.Thesis.Bullet":
                    Bullets.Remove( item as Bullet );
                    break;
                case "Assets.Thesis.Body":
                    int index = Agents.IndexOf( item as Body );
                    AgentLastPosition.RemoveAt( index );
                    Agents.RemoveAt( index );
                    break;
                default: return;
            }
        }
    }
}
