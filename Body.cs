﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Thesis
{
    public partial class Body : EntitySided, DamageReceiver
    {
        public Animator animator;

        [NonSerialized] public float horizontal;
        [NonSerialized] public float vertical;

        [NonSerialized] public Vector3 input_move = new Vector3();
        [NonSerialized] public Quaternion input_vertical_rotation = new Quaternion();
        [NonSerialized] public Quaternion input_horizontal_rotation = new Quaternion();

        [NonSerialized] public float sprint;
        [NonSerialized] public float walk;

        [NonSerialized] public float crouch;
        [NonSerialized] public bool jump;

        [NonSerialized] public float lean;

        [NonSerialized] public float ascending;

        [NonSerialized] protected float movement_factor;

        [NonSerialized] public float crouch_speed = 2f;
        [NonSerialized] public float normal_speed = 4f;
        [NonSerialized] public float walk_speed_modifier = -2.15f; //walk speed = normal_speed + walk_speed_modifier
        [NonSerialized] public float sprint_speed_modifier = 2f; //sprint speed = normal_speed + sprint_speed_modifier
        [NonSerialized] public float speed;

        [NonSerialized] public float jump_velocity = 5f;
        [NonSerialized] public float roll_velocity = 8.5f;

        [NonSerialized] public bool roll;          //curent state of roll
        [NonSerialized] public bool roll_user;     //user says if he want to roll
        [NonSerialized] public bool roll_animator; //animator says if body rolling and cant exit now

        [NonSerialized] public float fire1;
        [NonSerialized] public float fire2;
        [NonSerialized] public float fire3;

        [NonSerialized] public float stand_friction = 7.5f; 
        [NonSerialized] public float move_friction_modifier = 5f; //real move friction = stand_friction - move_friction_modifier
        [NonSerialized] public AnimationCurve friction_curve = new AnimationCurve( new Keyframe(0,0), new Keyframe(0.75f,0), new Keyframe(1,1) );


        [NonSerialized] protected float lean_angle = 55;
        public float LeanAngle { get { return lean_angle; } set { lean_angle = value; lean_right = Quaternion.Euler( 0, 0, lean_angle ); lean_left = Quaternion.Euler( 0, 0, -lean_angle );  } }
        protected Quaternion lean_right = Quaternion.Euler( 0, 0, -55 );
        protected Quaternion lean_left = Quaternion.Euler( 0, 0, 55 );

        Vector3 velocity_unchanged;
        Vector3 velocity_difference;
        Vector3 velocity_localphysics;

        protected bool update_type = false;
        public UpdateTypeEnum UpdateType { get { return update_type ? UpdateTypeEnum.Framed : UpdateTypeEnum.Fixed; } set { update_type = (value == UpdateTypeEnum.Framed); animator.updateMode = update_type ? AnimatorUpdateMode.Normal : AnimatorUpdateMode.AnimatePhysics; HandleLateUpdateType(); } }
        public float agentDeltaTime { get { return update_type ? Time.deltaTime : Time.fixedDeltaTime; } }


        [NonSerialized] public bool IsGrounded = true;

        public bool IsFire1 { get { return (fire1 != 0f); } }
        public bool IsFire2 { get { return (fire2 != 0f); } }
        public bool IsFire3 { get { return (fire3 != 0f); } }

        protected bool lateFixedUpdate = false;

        protected BodyInventory Inventory;

        protected virtual void Start()
        {
            GetComponents();

            //temp
            UpdateType = UpdateTypeEnum.Fixed;
            CurrentController = new BodyController_AI();
            //CurrentController = new BodyController_Player();
            //temp
            Inventory = new BodyInventory( this );
            AddAndEquipWeapon( Head.GetComponentInChildren<Weapon>() );

            CurrentController.Init( this );
        }

        protected virtual void Process()
        {
            //get internal info
            CollectInfo();
            //get controller info
            CurrentController.Process();

            RecalcInfo();

            movement_factor = (2 + sprint - walk);
            speed = Mathf.Lerp( ((sprint * sprint_speed_modifier) + (walk * walk_speed_modifier) + normal_speed), crouch_speed, crouch ); //movement_factor * normal_speed;

            HandleRoll();
            HandleGroundMovement();
            HandleRotation();
            HandleWeapon();

            //animation
            horizontal = input_move.x * movement_factor;
            vertical = input_move.z * movement_factor;

            animator.SetFloat( idX, horizontal );
            animator.SetFloat( idY, vertical );
            animator.SetFloat( idCrouch, crouch );
            animator.SetFloat( idAscending, ascending );
            animator.SetFloat( idPhyX, velocity_localphysics.x );
            animator.SetFloat( idPhyY, velocity_localphysics.y );
            animator.SetFloat( idPhyZ, velocity_localphysics.z );
            animator.SetBool( idGrounded, IsGrounded );
            animator.SetBool( idRoll, roll );
        }

        
        protected virtual void FixedUpdate()
        {
            CurrentController.FixedUpdate();

            if( !update_type )
                Process();

            PhyMaterial.dynamicFriction = stand_friction - (friction_curve.Evaluate( input_move.magnitude ) * move_friction_modifier);
            lateFixedUpdate = true;
        }

        protected virtual void Update()
        {
            CurrentController.Update();

            HandleRotation();

            if( update_type )
                Process();
        }

        protected virtual void LateUpdate()
        {
            //if( !update_type && !lateFixedUpdate )
            //    return;
            if( !update_type )
                return;
            
            CurrentController.LateUpdate();

            Spine.localRotation = (lean < 0) ? Quaternion.Lerp( Spine.localRotation, lean_left, -lean ) : Quaternion.Lerp( Spine.localRotation, lean_right, lean );
            Neck.rotation = transform.rotation * input_vertical_rotation;

            lateFixedUpdate = false;
        }

        protected virtual void LateFixedUpdate()
        {
            CurrentController.LateUpdate();

            Spine.localRotation = (lean < 0) ? Quaternion.Lerp( Spine.localRotation, lean_left, -lean ) : Quaternion.Lerp( Spine.localRotation, lean_right, lean );
            Neck.rotation = transform.rotation * input_vertical_rotation;
        }
    }
}

//protected virtual void Awake()
//{
//    CurrentController.Awake();
//}



//protected virtual void Update()
//{
//    CurrentController.Update();
//}

//protected virtual void FixedUpdate()
//{
//    CurrentController.FixedUpdate();
//}

//protected virtual void LateUpdate()
//{
//    CurrentController.LateUpdate();
//}

//protected virtual void OnAnimatorIK( int layerIndex )
//{

//}