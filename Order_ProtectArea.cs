﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Thesis
{
    public class Order_ProtectArea : Order
    {
        public Order_ProtectArea( Vector3[] area = null, Order nextorder = null ) : base(nextorder) { Area = area; }

        public Vector3[] Area;
    }
}
