﻿using UnityEngine;

namespace Assets.Thesis
{
    public class EntitySided : Entity
    {
        private static readonly char substractor = ' ';

        public uint Side { get; private set; }

        public void SetSide( string side )
        {
            if( side.Length < 4 )
                for( ; side.Length < 4; )
                    side += substractor;

            uint result = 0;
            uint digit = 1;
            for( int i = 0; i < 4; i++ )
            {
                char ch = side[i];
                int preres = (ch - substractor);
                result += (uint)preres * digit;
                digit *= 100;
            }
            Side = result;
        }

        public string GetSide()
        {
            string result = string.Empty;
            uint temp = Side;
            for( int i = 0; i < 4; i++ )
            {
                uint leftover = (temp % 100);
                if( leftover > substractor )
                    result += (char)(leftover + substractor);
                temp /= 100;
            }
            return result;
        }

        protected override void Awake()
        {
            base.Awake();
            EnvironmentManager.Sideds.Add( this );
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            EnvironmentManager.Sideds.Remove( this );
        }
    }
}
