﻿//using System;
using System.Collections.Generic;
using System.Linq;
//using System.Text;
using UnityEngine;
using Assets.Thesis.Extends;

namespace Assets.Thesis
{
    public enum BulletType
    {
        BT_Default = 0,
        BT_9x19,
        BT_556x45,
        BT_762x51,
        BT_127x99,
        BT_Count //must be last
    }

    public static class AmmoManager
    {
        static Bullet[] BulletPrefabs;
        static Transform[] BulletDecals;
        static Shell[] BulletShells;
        static Dictionary<string, Clip> Clips;
        public static readonly int bulletLayerMask;
        public static readonly int ignoreBulletLayerMask;
        public static readonly int bodyLayerMask;
        public static readonly int ignoreBodyLayerMask;

        static AmmoManager()
        {
            bulletLayerMask = (1 << LayerMask.NameToLayer( "Bullet" ));
            ignoreBulletLayerMask = ~bulletLayerMask;

            bodyLayerMask = (1 << LayerMask.NameToLayer( "Body" ));
            ignoreBodyLayerMask = ~bodyLayerMask;

            BulletPrefabs = new Bullet[(int)BulletType.BT_Count];
            BulletDecals = new Transform[(int)BulletType.BT_Count];
            BulletShells = new Shell[(int)BulletType.BT_Count];


            BulletPrefabs = Resources.LoadAll<Bullet>( @"Thesis\Ammo\Bullets" );
            BulletDecals = Resources.LoadAll<Transform>( @"Thesis\Ammo\Decals" );
            BulletShells = Resources.LoadAll<Shell>( @"Thesis\Ammo\Shells" );

            LoadClips( Resources.LoadAll<Clip>( @"Thesis\Ammo\Clips" ) );
            Sort();
        }

        public static Bullet ShootBullet( Vector3 position, Quaternion rotation, BulletType type )
        {
            return Object.Instantiate( BulletPrefabs[(int)type], position, rotation );
        }

        public static Shell ShootBullet( Vector3 position, Quaternion rotation, Bullet bullet )
        {
            Shell shell = bullet.Shell;

            bullet.transform.parent = null;
            bullet.transform.SetPositionAndRotation( position, rotation );
            bullet.Shell = null;
            bullet.enabled = true;

            return shell;
        }

        public static Bullet SpawnBullet( Vector3 position, Quaternion rotation, BulletType type, Transform parent = null, bool local = false )
        {
            Bullet bullet = Object.Instantiate( BulletPrefabs[(int)type], position, rotation, parent );
            bullet.enabled = false;

            if( parent != null && local )
                bullet.transform.SetLocalPositionAndRotation( position, rotation );

            return bullet;
        }

        public static Bullet SpawnCartrige( Vector3 position, Quaternion rotation, BulletType type, Transform parent = null, bool local = false )
        {
            Bullet bullet = SpawnBullet( position, rotation, type, parent, local );
            bullet.Shell = SpawnShell( Vector3.zero, Quaternion.identity, type, bullet.transform, true );
            bullet.Shell.collider.enabled = false;
            bullet.Shell.rigidbody.isKinematic = true;

            return bullet;
        }

        public static Transform DrawDecal( RaycastHit hit, BulletType type )
        {
            return DrawDecal( hit.point + (hit.normal * 0.01f), Quaternion.FromToRotation( Vector3.back, hit.normal ), type, hit.transform );
        }

        public static Transform DrawDecal( Vector3 position, Quaternion rotation, BulletType type, Transform parent = null )
        {
            return Object.Instantiate( BulletDecals[(int)type], position, rotation, parent );
        }

        public static Shell SpawnShell( Vector3 position, Quaternion rotation, BulletType type, Transform parent = null, bool local = false )
        {
            Shell shell = Object.Instantiate( BulletShells[(int)type], position, rotation, parent );

            if( parent != null && local )
                shell.transform.SetLocalPositionAndRotation( position, rotation );

            return shell;
        }

        public static Shell EjectBrass( Vector3 position, Quaternion rotation, Vector3 direction, BulletType type )
        {
            Shell shell = Object.Instantiate( BulletShells[(int)type], position, rotation );
            shell.rigidbody.AddForce( direction, ForceMode.Impulse );
            return shell;
        }

        public static Shell EjectBrass( Vector3 position, Quaternion rotation, Vector3 direction, Shell shell )
        {
            shell.transform.SetPositionAndRotation( position, rotation );
            shell.transform.parent = null;
            shell.collider.enabled = true;
            shell.rigidbody.isKinematic = false;
            shell.rigidbody.AddForce( direction, ForceMode.Impulse );
            return shell;
        }

        public static Clip SpawnClip( Vector3 position, Quaternion rotation, string type, Transform parent = null )
        {
            return Object.Instantiate( Clips[type], position, rotation, parent ); 
        }

        private static void LoadClips( Clip[] clips )
        {
            Clips = new Dictionary<string, Clip>();
            for( int i = 0; i < clips.Length; i++ )
                Clips.Add( clips[i].name, clips[i] );
        }

        private static void Sort()
        {   
            BulletType[] types = System.Enum.GetValues( typeof( BulletType ) ).Cast<BulletType>().ToArray();
            BulletPrefabs = BulletPrefabs.OrderBy( x => System.Array.IndexOf( types, x.type ) ).ToArray();

        //    for( int i = 0; i < BulletPrefabs.Length; i++ )
        //    {
        //        if( BulletPrefabs[i].type != types[i] )
        //        {
        //            for( int j = 0; j < BulletPrefabs.Length; j++ )
        //            {
        //                if( BulletPrefabs[j].type == types[i] )
        //                {
        //                    Extend.Swap( ref BulletPrefabs[i], ref BulletPrefabs[j] );
        //                    break;
        //                }
        //            }
        //        }
        //    }
        }

        public static float CalcDanger( BulletType type ) { return CalcDanger( BulletPrefabs[(int)type] ); }
        public static float CalcDanger( Bullet bullet )
        {
            if( bullet == null )
                return 0;

            return bullet.GetDanger();
        }
    }
}
