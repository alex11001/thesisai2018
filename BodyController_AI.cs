﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.AI;

namespace Assets.Thesis
{
    public class BodyController_AI : BodyController
    {
        public Behavior CurrentBehavior;
        protected List<Behavior> InitializedBehaviors = new List<Behavior>();
        public  List<MemoryBlock> Memory = new List<MemoryBlock>();

        public Order CurrentOrder;

        public NavMeshAgentShell NavAgent;
        public Camera View;
        public Plane[] Frustum = new Plane[6];

        public byte ShouldLean = 0;
        protected float lean = 0;

        public bool ShouldCrouch = false;
        protected float crouch = 0;

        public bool ShouldSprint = false;
        protected float sprint = 0;

        public bool ShouldWalk = false;
        protected float walk = 0;

        public bool ShouldFire1 = false;
        protected float fire1 = 0;

        public bool ShouldFire2 = false;
        protected float fire2 = 0;

        public bool ShouldFire3 = false;
        protected float fire3 = 0;

        public bool ShouldReload = false;

        Vector3 move = new Vector3();

        BodyControllerStateInfoAI StateInfoAI { get { return (BodyControllerStateInfoAI)StateInfo; } }

        public override void Init( Body body, bool force_reinit = false )
        {
            bool isinit = IsInitilized;
            base.Init( body, force_reinit );

            StateInfo = new BodyControllerStateInfoAI( this );
            NavAgent = new NavMeshAgentShell( body.GetComponent<NavMeshAgent>() );
            View = body.Head.GetComponentInChildren<Camera>();

            //CurrentBehavior = new Behavior_Default();
            CurrentBehavior = new Behavior_Test();
            CurrentBehavior.Controller = this;
            CurrentBehavior.Begin();

            if( !isinit || force_reinit )
                Start();
        }

        public override void Process()
        {
            UpdateMemory();

            base.Process();
            CurrentBehavior.Process();

            Body.lean = lean = Mathf.Lerp( lean, ShouldLean, Body.agentDeltaTime * 7.5f );
            Body.walk = walk = Mathf.Lerp( walk, ShouldWalk ? 1 : 0, Body.agentDeltaTime * 7.5f );
            Body.sprint = sprint = Mathf.Lerp( sprint, ShouldSprint ? 1 : 0, Body.agentDeltaTime * 7.5f );
            Body.crouch = crouch = Mathf.Lerp( crouch, ShouldCrouch ? 1 : 0, Body.agentDeltaTime * 7.5f );

            Body.fire1 = fire1 = Mathf.Lerp( fire1, ShouldFire1 ? 1 : 0, Body.agentDeltaTime * 7.5f );
            Body.fire2 = fire2 = Mathf.Lerp( fire2, ShouldFire2 ? 1 : 0, Body.agentDeltaTime * 7.5f );
            Body.fire3 = fire3 = Mathf.Lerp( fire3, ShouldFire3 ? 1 : 0, Body.agentDeltaTime * 7.5f );


            Vector3 dir = NavAgent.agent.desiredVelocity;
            move = Vector3.Lerp( move, (dir.magnitude > 0.1f && !NavAgent.IsReachDestination) ? Body.transform.InverseTransformDirection( dir ) : Vector3.zero, Body.agentDeltaTime * 20f );

            Body.input_move = move;

            if( ShouldReload )
            {
                Body.ReloadWeapon();
                ShouldReload = false;
            }
        }

        public override void Awake() { base.Awake(); }
        public override void Start() { base.Start(); }
        public override void Update() { base.Update(); }
        public override void FixedUpdate() { base.FixedUpdate(); }

        public override void LateUpdate()
        {
            base.LateUpdate();
            Body.lean = lean;
        }

        //call from Process only
        public virtual void LookAt( Transform target ) { LookAt( target.position ); }
        public virtual void LookAt( Vector3 target )
        {
            Weapon EquipedWeapon = Body.EquipedWeapon;
            if( EquipedWeapon != null )
            {
                Quaternion inverse_weapon = Quaternion.Inverse( EquipedWeapon.transform.localRotation );
                Quaternion inverse_sight = EquipedWeapon.Sight.localRotation;
                Quaternion targetLook = Quaternion.LookRotation( target - EquipedWeapon.Sight.position );//Controlled.Head.position );//Controlled.transform.position );
                targetLook = targetLook * inverse_sight * inverse_weapon;
                Body.input_horizontal_rotation = Quaternion.Euler( 0, targetLook.eulerAngles.y, 0 );
                Vector3 targetLookEuler = targetLook.eulerAngles; targetLookEuler.y = 0;
                Body.input_vertical_rotation = Quaternion.Euler( targetLookEuler );
            }
            else
            {
                Body.input_horizontal_rotation = Body.input_vertical_rotation = Quaternion.LookRotation( target - Body.Head.position );
            }
        }

        public virtual void LookAtDir( Vector3 target ) { Body.input_vertical_rotation = Quaternion.identity; Body.input_horizontal_rotation = Quaternion.LookRotation( target ); }

        public virtual void MoveTo( Transform target ) { MoveTo( target.position ); }
        public virtual void MoveTo( Vector3 target )
        {
            NavAgent.MoveTo( target );
        }
        //end of call from Process only

        public virtual Type GetBehavior() { return (CurrentBehavior == null) ? null : CurrentBehavior.GetType(); }
        public virtual void SetBehavior( Type type, bool reinit = false )
        {
            Behavior old = InitializedBehaviors.Find( x => x.GetType() == type );
            if( reinit && old != null )
            {
                InitializedBehaviors.Remove( old );
                old = null;
            }

            if( old == null )
            {
                Behavior newb = Activator.CreateInstance( type, Body, this ) as Behavior;
                InitializedBehaviors.Add( newb );
                if( CurrentBehavior != null )
                    CurrentBehavior.SwitchBehavior( newb );
                else
                    CurrentBehavior = newb;
            }
            else
                CurrentBehavior.SwitchBehavior( old );
        }

        public virtual void RecalcFrustum()
        {
            GeometryUtility.CalculateFrustumPlanes( View, Frustum );
        }

        protected virtual void UpdateMemory()
        {
            RecalcFrustum();
            Entity[] seen = EnvironmentManager.GetSeenEntities( Frustum );

            for( int i = 0; i < seen.Length; i++ )
            {
                Body agent = seen[i] as Body;
                if( agent == null )
                    continue;

                MemoryBlock_Agent memory_about = Memory.Find( x => (x is MemoryBlock_Agent && ((MemoryBlock_Agent)x).Agent == agent) ) as MemoryBlock_Agent;
                if( memory_about == null )
                {
                    Memory.Add( new MemoryBlock_Agent() );
                    memory_about = Memory[Memory.Count - 1] as MemoryBlock_Agent;
                    memory_about.Agent = agent;
                }

                memory_about.HitsCount += EnvironmentManager.RegistredHits.Where( x => x.Reciever == agent ).ToArray().Length;

            }
        }
    }
}
