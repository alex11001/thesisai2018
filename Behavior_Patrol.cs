﻿using UnityEngine;

namespace Assets.Thesis
{
    public class Behavior_Patrol : Behavior
    {
        int route_index;

        public Behavior_Patrol( Body body, BodyController_AI controller ) : base( body, controller ) { }

        public override void Begin()
        {
            if( controller.CurrentOrder == null )
                controller.CurrentOrder = new Order_Default();

            Order_Patrol order = controller.CurrentOrder as Order_Patrol;

            if( order != null )
            {
                route_index = 0;
                controller.MoveTo( order.PatrolRoute[route_index] );
                controller.ShouldWalk = true;
            }
        }

        public override void Process()
        {
            if( controller.CurrentOrder != null )
            {
                Order_Patrol order = controller.CurrentOrder as Order_Patrol;
                if( order != null )
                {
                    if( order.PatrolRoute.Length > 0 && (controller.NavAgent.IsReachDestination) )
                    {
                        route_index = (route_index + 1) % order.PatrolRoute.Length;
                        controller.MoveTo( order.PatrolRoute[route_index] );
                        controller.ShouldWalk = true;
                    }
                }
                else
                    controller.SetBehavior( GetBehaviorFitOrder( controller.CurrentOrder ) );
            }
        }

        public static new float Priority( BodyController controller )
        {
            return (!controller.StateInfo.IsInCombat && !controller.StateInfo.IsAlert && controller.StateInfo.IsShouldPatrol) ? 1 : 0;
        }
    }
}
