﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Assets.Thesis.Extends;

namespace Assets.Thesis
{
    public partial class Body : EntitySided, DamageReceiver
    {
        [NonSerialized] public float GroundCheckDistanceLong = 0.25f;
        [NonSerialized] public float GroundCheckDistanceShort = 0.2f;
        [NonSerialized] public float GroundCheckDistanceExtend = 1f;

        protected AnimatorStateInfo animator_curstateinfo;
        protected AnimatorStateInfo animator_nextstateinfo;


        protected RaycastHit GroundLongCenterHit;     protected bool GroundLongCenterResult = false;
        protected RaycastHit GroundShortCenterHit;    protected bool GroundShortCenterResult = false;

        protected RaycastHit GroundStraightHit;       protected bool GroundStraightResult = false;
        protected RaycastHit GroundSideHit;           protected bool GroundSideResult = false;


        protected virtual void CollectInfo()
        {
            animator_curstateinfo = animator.GetCurrentAnimatorStateInfo( 0 );
            animator_nextstateinfo = animator.GetNextAnimatorStateInfo( 0 );

            velocity_unchanged = rigidbody.velocity;
            velocity_localphysics = transform.InverseTransformDirection( velocity_unchanged );

            GroundCheckDistanceExtend = rigidbody.velocity.y < 5 ? 1 : 8;

            Vector3 shift_up = (Vector3.up * 0.1f * GroundCheckDistanceExtend);
            Vector3 shift_down = Vector3.down * GroundCheckDistanceLong * GroundCheckDistanceExtend;
            
            GroundLongCenterResult = Physics.Raycast( transform.position + shift_up, Vector3.down, out GroundLongCenterHit, GroundCheckDistanceLong * GroundCheckDistanceExtend );
            GroundShortCenterResult = Physics.Raycast( transform.position + shift_up, Vector3.down, out GroundShortCenterHit, GroundCheckDistanceShort * GroundCheckDistanceExtend );

            GroundStraightResult = Physics.Raycast( transform.position + (GroundCheckerStraight * input_move.z) + (/*(input_move.z == 0*/ Extend.NearlyEquals( 0, input_move.z, 0.05f ) ? GroundCheckerStraight: Vector3.zero) + shift_up, Vector3.down, out GroundStraightHit, GroundCheckDistanceLong * GroundCheckDistanceExtend );
            GroundSideResult = Physics.Raycast( transform.position + (GroundCheckerSide * input_move.x) + (/*(input_move.x == 0)*/ Extend.NearlyEquals( 0, input_move.x, 0.05f ) ? GroundCheckerSide : Vector3.zero) + shift_up, Vector3.down, out GroundSideHit, GroundCheckDistanceLong * GroundCheckDistanceExtend );

            IsGrounded = jump ? GroundShortCenterResult : GroundLongCenterResult;

            Debug.DrawLine( transform.position + shift_up, transform.position + shift_up + shift_down );
        }

        protected Vector3 feet_normal;

        protected virtual void RecalcInfo()
        {
            Vector3 straight = GroundStraightHit.point;
            Vector3 side = GroundSideHit.point;
            Vector3 center = GroundLongCenterHit.point;

            Debug.DrawLine( transform.position, straight );
            Debug.DrawLine( side, straight );
            Debug.DrawLine( transform.position, side );

            feet_normal = Vector3.Cross( straight - center, side - center );

            Debug.DrawLine( center, center + feet_normal, Color.yellow );

            feet_normal.x = Mathf.Abs( feet_normal.x );
            feet_normal.y = Mathf.Abs( feet_normal.y );
            feet_normal.z = Mathf.Abs( feet_normal.z );
            float Angle = Vector3.Angle( Vector3.up, feet_normal );

            ascending = ((straight.y < transform.position.y)?-1:1) *
                        //((side.y < transform.position.y) ? -1 : 1) *
                        Mathf.Clamp( Angle, -30, 30 ) / 30;
        }

        protected Vector2 input_roll = new Vector2();
        protected virtual void HandleRoll()
        {
            roll = IsGrounded && roll_user;

            if( !IsGrounded )
                return;

            if( (animator_curstateinfo.shortNameHash != idRoll) && roll_user )
            {
                if( input_move.x == 0 && input_move.z == 0 )
                    input_move.z = 1f;
                else
                {
                    if( input_move.x < 0 )
                        input_move.x = -1f;
                    else if( 0 < input_move.x )
                        input_move.x = 1f;

                    if( input_move.z < 0 )
                        input_move.z = -1f;
                    else if( 0 < input_move.z )
                        input_move.z = 1f;
                }
                input_roll = input_move;
            }

            roll_animator = animator_curstateinfo.shortNameHash == idRoll && !animator.IsInTransition( 0 );

            if( roll_animator )
                input_move = input_roll;

            //roll = roll_user; 

            if( animator_nextstateinfo.shortNameHash == idRoll )
            {
                velocity_unchanged = rigidbody.velocity;

                
                float magnitude_factor = (input_move.magnitude > 0) ? (roll_velocity / input_move.magnitude) : 1;

                move.x = input_move.x;
                move.z = input_move.z;

                move = transform.rotation * move;

                move.Normalize();

                float velocity_limits = (roll_velocity - Mathf.Abs( velocity_unchanged.x )) * magnitude_factor;
                velocity_difference.x = (velocity_limits > 0) ? Mathf.Clamp( move.x * roll_velocity, -velocity_limits, velocity_limits ) : 0;

                velocity_limits = (roll_velocity - Mathf.Abs( velocity_unchanged.z )) * magnitude_factor;
                velocity_difference.z = (velocity_limits > 0) ? Mathf.Clamp( move.z * roll_velocity, -velocity_limits, velocity_limits ) : 0;

                //velocity_difference = transform.rotation * velocity_difference;

                rigidbody.velocity += velocity_difference;
            }
        }

        protected Vector3 move = new Vector3();
        protected virtual void HandleGroundMovement()
        {
            if( roll_animator || roll_user )
                return;

            //float magnitude_factor = (input.magnitude > 0) ? (speed / input.magnitude) : 1;

            move = transform.rotation * input_move * speed;

            velocity_difference = (move - velocity_unchanged);
            velocity_difference.x = (((move.x > 0) && (move.x > velocity_unchanged.x)) || ((move.x < 0) && (move.x < velocity_unchanged.x))) ? velocity_difference.x : 0;
            velocity_difference.y = 0;
            //velocity_difference.y = (((move.y > 0) && (move.y > velocity_unchanged.y)) || ((move.y < 0) && (move.y < velocity_unchanged.y))) ? velocity_difference.y : 0;
            velocity_difference.z = (((move.z > 0) && (move.z > velocity_unchanged.z)) || ((move.z < 0) && (move.z < velocity_unchanged.z))) ? velocity_difference.z : 0;


            //move.Normalize();

            //move = //(transform.TransformDirection( new Vector3( input.x, 0, input.y ) ));
            //float 
            //magnitude_factor = move.sqrMagnitude - velocity_unchanged.sqrMagnitude;//(input.magnitude > 0) ? speed / input.magnitude : 1;//(input.magnitude > 0) ? (speed / input.magnitude) : 1;
            //magnitude_factor = (magnitude_factor > 0) ? speed / magnitude_factor : 0;
            //velocity_difference = (magnitude_factor <= 1) ? move * magnitude_factor * speed : Vector3.zero;

            //move = Vector3.ProjectOnPlane( move, feet_normal );

            //float velocity_limits = (speed - Mathf.Abs( velocity_unchanged.x )) * magnitude_factor;
            //velocity_difference.x = (velocity_limits > 0) ? Mathf.Clamp( move.x * 1.5f, -velocity_limits, velocity_limits ) : 0;

            //velocity_limits = (speed - Mathf.Abs( velocity_unchanged.z )) * magnitude_factor;
            //velocity_difference.z = (velocity_limits > 0) ? Mathf.Clamp( move.z * 1.5f, -velocity_limits, velocity_limits ) : 0;

            //velocity_difference = transform.rotation * velocity_difference;

            //velocity_difference = transform.InverseTransformDirection( velocity_difference );
            //velocity_difference = Vector3.ProjectOnPlane( velocity_difference, feet_normal );

            velocity_difference.y = (IsGrounded && jump) ? jump_velocity : 0;

            rigidbody.velocity += velocity_difference;
        }

        protected virtual void HandleSlide()
        {

        }

        protected virtual void HandleRotation()
        {
            transform.localRotation = input_horizontal_rotation;
        }

        protected virtual void HandleWeapon()
        {
            if( Inventory.EquipedWeapon != null )
            {
                Inventory.EquipedWeapon.fire = IsFire1;
            }    
        }
    }
}


//RaycastHit GroundLongForwardHit;    bool GroundLongForwardResult = false;
//RaycastHit GroundShortForwardHit;   bool GroundShortForwardResult = false;

//RaycastHit GroundLongBackwardHit;   bool GroundLongBackwardResult = false;
//RaycastHit GroundShortBackwardHit;  bool GroundShortBackwarResult = false;

//RaycastHit GroundLongLeftHit;       bool GroundLongLeftResult = false;
//RaycastHit GroundShortLeftHit;      bool GroundShortLeftResult = false;

//RaycastHit GroundLongRightHit;      bool GroundLongRightResult = false;
//RaycastHit GroundShortRightHit;     bool GroundShortRightResult = false;


//Debug.DrawLine( transform.position + shift_up, transform.position + shift_up + shift_down );

//Debug.DrawLine( transform.position + shift_up + GroundCheckerStraight, transform.position + shift_up + shift_down + GroundCheckerStraight, Color.red );
//Debug.DrawLine( transform.position + shift_up + -GroundCheckerStraight, transform.position + shift_up + shift_down + -GroundCheckerStraight, Color.blue );
//Debug.DrawLine( transform.position + shift_up + GroundCheckerSide, transform.position + shift_up + shift_down + GroundCheckerSide, Color.green );
//Debug.DrawLine( transform.position + shift_up + -GroundCheckerSide, transform.position + shift_up + shift_down + -GroundCheckerSide, Color.cyan );

//GroundLongForwardResult = Physics.Raycast( transform.position + GroundCheckerStraight + shift_up, Vector3.down, out GroundLongForwardHit, GroundCheckDistanceLong * GroundCheckDistanceExtend );
//GroundShortForwardResult = Physics.Raycast( transform.position + GroundCheckerStraight + shift_up, Vector3.down, out GroundShortForwardHit, GroundCheckDistanceShort * GroundCheckDistanceExtend );

//GroundLongBackwardResult = Physics.Raycast( transform.position + -GroundCheckerStraight + shift_up, Vector3.down, out GroundLongBackwardHit, GroundCheckDistanceLong * GroundCheckDistanceExtend );
//GroundShortBackwarResult = Physics.Raycast( transform.position + -GroundCheckerStraight + shift_up, Vector3.down, out GroundShortBackwardHit, GroundCheckDistanceShort * GroundCheckDistanceExtend );

//GroundLongLeftResult = Physics.Raycast( transform.position + -GroundCheckerSide + shift_up, Vector3.down, out GroundLongLeftHit, GroundCheckDistanceLong * GroundCheckDistanceExtend );
//GroundShortLeftResult = Physics.Raycast( transform.position + -GroundCheckerSide + shift_up, Vector3.down, out GroundShortLeftHit, GroundCheckDistanceShort * GroundCheckDistanceExtend );

//GroundLongRightResult = Physics.Raycast( transform.position + GroundCheckerSide + shift_up, Vector3.down, out GroundLongRightHit, GroundCheckDistanceLong * GroundCheckDistanceExtend );
//GroundShortRightResult = Physics.Raycast( transform.position + GroundCheckerSide + shift_up, Vector3.down, out GroundShortRightHit, GroundCheckDistanceShort * GroundCheckDistanceExtend );