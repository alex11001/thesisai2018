﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Thesis
{
    public class Clip : Entity
    {
        [Serializable] class IndexedPosRot { public int index; public PositionRotation initial; public PositionRotation shift_even; public PositionRotation shift_odd; public PositionRotation shift( int index ) { return (index % 2 == 0) ? shift_even : shift_odd; } }
        [Serializable] class IndexedBulletType { public int index; public BulletType type; }
        [Serializable] class BulletsPattern { public List<IndexedPosRot> PositionsPattern; public List<IndexedBulletType> TypesPattern; }

        public int DefaultMaxCount = 15;
        public int MaxAmmoCount { get { return Bullets.MaxCount; } set { Bullets.MaxCount = value; } }
        public int AmmoCount { get { return Bullets.Count; } }

        protected Transform[] BulletPositions;
        public FixedQueue<Bullet> Bullets { get; protected set; }
        //public List<Bullet> Bullets { get; protected set; }

        public BulletType DefaultBulletType = BulletType.BT_Default;
        public BulletType BulletType { get { return (Bullets.Count > 0) ? Bullets.Peek().type : DefaultBulletType; } }

        [SerializeField] BulletsPattern BulletPattern;

        protected override void Awake()
        {
            collider = GetComponent<Collider>();
            rigidbody = GetComponent<Rigidbody>();
            InitBullets();
        }

        public Bullet DequeueBullet()
        {
            if( Bullets.Count == 0 )
                return null;

            Bullet bullet = Bullets.Dequeue();
            DefaultBulletType = bullet.type;

            for( int i = 0; i < Bullets.Count; i++ )
            {
                Transform bullet_tranform = Bullets[i].transform;
                bullet_tranform.parent = BulletPositions[i];
                bullet_tranform.localPosition = Vector3.zero;
                bullet_tranform.localRotation = Quaternion.identity;
            }

            return bullet;
        }

        public virtual void Pickup( Transform newparent = null )
        {
            if( newparent != null )
                transform.parent = newparent;

            collider.enabled = false;
            rigidbody.isKinematic = true;
        }

        public virtual void Drop()
        {
            transform.parent = null;
            collider.enabled = true;
            rigidbody.isKinematic = false;
        }

        protected virtual void InitBullets()
        {
            Transform root = null;
            foreach( Transform t in transform )
                if( t.name == "Bullets" ) { root = t; break; }

            bool init_pos = (root == null);

            if( init_pos )
            {
                root = new GameObject( "Bullets" ).transform;
                root.parent = transform;
                root.localPosition = Vector3.zero;
                root.localRotation = Quaternion.identity;
            }  

            if( BulletPattern != null && BulletPattern.PositionsPattern != null && BulletPattern.PositionsPattern.Count > 0 )
                InitWithPattern( root );
            else
                InitWithPositions( root, init_pos );
        }

        protected virtual void InitWithPositions( Transform BulletsRoot, bool init_positions )
        {
            if( init_positions )
            {
                Vector3 shift = new Vector3( 0, -0.1f );

                for( int i = 0; i < DefaultMaxCount; i++ )
                {
                    Transform t = new GameObject( "bullet_position" + i.ToString() ).transform;
                    t.parent = BulletsRoot;
                    t.localPosition = shift * i;
                    t.localRotation = Quaternion.identity;
                }
            }

            List<Transform> bullet_positions = new List<Transform>();
            List<Bullet> bullets_inself = new List<Bullet>();
            Transform[] bullet_info = BulletsRoot.Cast<Transform>().ToArray();
            for( int i = 0; i < bullet_info.Length; i++ )
            {
                Transform bullet_position = bullet_info[i];
                Bullet bullet = bullet_position.GetComponent<Bullet>();
                if( bullet == null )
                {
                    if( bullet_position.childCount > 0 )
                    {
                        bullet = bullet_position.GetChild( 0 ).GetComponent<Bullet>();
                        if( bullet != null )
                        {
                            bullets_inself.Add( bullet );
                            bullet_positions.Add( bullet_position );
                            continue;
                        }
                        else
                            bullet_position.DetachChildren();
                    }

                    bullets_inself.Add( AmmoManager.SpawnBullet( bullet_position.position, bullet_position.rotation, DefaultBulletType, bullet_position ) );
                    bullet_positions.Add( bullet_position );
                }
                else
                {
                    bullets_inself.Add( bullet );
                    Transform new_bullet_position = new GameObject( "bullet_position" + i.ToString() ).transform;
                    new_bullet_position.parent = BulletsRoot;
                    new_bullet_position.SetPositionAndRotation( bullet_position.position, bullet_position.rotation );
                    new_bullet_position.SetSiblingIndex( bullet_position.GetSiblingIndex() );

                    bullet_position.parent = new_bullet_position;
                    bullet_position.localPosition = Vector3.zero;
                    bullet_position.localRotation = Quaternion.identity;

                    bullet_positions.Add( new_bullet_position );
                }
            }

            BulletPositions = bullet_positions.ToArray();
            Bullets = new FixedQueue<Bullet>( bullets_inself, bullets_inself.Count );
        }

        protected virtual void InitWithPattern( Transform root )
        {
            BulletPattern.PositionsPattern.Sort( (x,y) => (y.index < x.index ? 1 : -1) );

            if( !(BulletPattern.TypesPattern != null && BulletPattern.TypesPattern.Count > 0) )
                BulletPattern.TypesPattern = new List<IndexedBulletType> { new IndexedBulletType { index = 0, type = BulletType.BT_Default } };
            else
                BulletPattern.TypesPattern.Sort( ( x, y ) => (y.index < x.index ? 1 : -1) );

            int next_position_index = (BulletPattern.PositionsPattern.Count > 1) ? BulletPattern.PositionsPattern[1].index : -1;
            int next_type_index = (BulletPattern.TypesPattern.Count > 1) ? BulletPattern.TypesPattern[1].index : -1;

            List<Transform> bullet_positions = new List<Transform>();
            List<Bullet> bullets = new List<Bullet>();

            for( int i = 0, p = 0, pn = 0, t = 0; i < DefaultMaxCount; i++, pn++ )
            {
                if( i == next_position_index && BulletPattern.PositionsPattern.Count > ++p + 1 + (pn = 0) )
                    next_position_index = BulletPattern.PositionsPattern[p + 1].index;

                if( i == next_type_index && BulletPattern.TypesPattern.Count > ++t + 1 )
                    next_type_index = BulletPattern.TypesPattern[t + 1].index;

                Transform cartridge_position = new GameObject( "bullet_position" + i.ToString() ).transform;
                cartridge_position.SetPositionAndRotation(
                                      root.position + BulletPattern.PositionsPattern[p].initial.position + BulletPattern.PositionsPattern[p].shift(i).position * pn,
                    root.rotation * Quaternion.Euler( BulletPattern.PositionsPattern[p].initial.rotation + BulletPattern.PositionsPattern[p].shift(i).rotation * pn ) );
                cartridge_position.parent = root;

                Bullet cartridge = AmmoManager.SpawnBullet( Vector3.zero, Quaternion.identity, BulletPattern.TypesPattern[t].type, cartridge_position, true );

                bullet_positions.Add( cartridge_position );
                bullets.Add( cartridge );
            }

            BulletPositions = bullet_positions.ToArray();
            Bullets = new FixedQueue<Bullet>( bullets, bullets.Count );
        }
    }
}
