﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Thesis
{
    [DefaultExecutionOrder( -50 )]
    public partial class EnvironmentManager : MonoBehaviour
    {
        public static float HeatCheckRadius = 3;
        public static float HeatReduceByStep = 0.03f; //in %

        [RuntimeInitializeOnLoadMethod( RuntimeInitializeLoadType.BeforeSceneLoad )]
        protected static void Init()
        {
            Entities = new List<Entity>();
            Sideds = new List<EntitySided>();
            Agents = new List<Body>();
            Bullets = new List<Bullet>();
            RegistredHits = new List<HitInfo>();

            GameObject parent = new GameObject( "EnvironmentManager", typeof( EnvironmentManager ) );
        }

        protected virtual void Awake() { UpdateInfo(); }
        protected virtual void FixedUpdate() { UpdateInfo(); }

        protected virtual void UpdateInfo()
        {
            RegistredHits.Clear();

            float bullets_atall = Bullets.Count;
            float agents_atall = Agents.Count;

            for( int i = 0; i < agents_atall; i++ )
            {
                Body agent = Agents[i];
                AgentLastPosition[i].Enqueue( agent.transform.position );

                float bullets_around = Physics.OverlapSphereNonAlloc( agent.transform.position, HeatCheckRadius, null, AmmoManager.bulletLayerMask );

                agent.CurrentController.StateInfo.Heat = (-(agent.CurrentController.StateInfo.Heat * HeatReduceByStep) + (bullets_around / bullets_atall)) / agents_atall;
            }
        }

        public static Vector3[] GetSafeRoute( Body me, Body enemy ) { return GetSafeRoute( me.transform.position, enemy.transform.position, me.Side ); }
        public static Vector3[] GetSafeRoute( Vector3 start, Vector3 end, uint side )
        {
            return null; //temp
        }

        public static Vector3 GetCover( Body body ) { return GetCover( body.transform.position, body.Side ); }
        public static Vector3 GetCover( Vector3 current_position, uint side )
        {
            return new Vector3(); //temp
        }

        public static Entity[] GetSeenEntities( Plane[] Frustum )
        {
            List<Entity> seen = new List<Entity>();
            for( int i = 0; i < Entities.Count; i++ )
            {
                Entity entity = Entities[i];
                if( entity.collider == null )
                    continue;

                if( GeometryUtility.TestPlanesAABB( Frustum, entity.collider.bounds ) )
                    seen.Add( entity );
            }

            return seen.ToArray();
        }
    }
}
