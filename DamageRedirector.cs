﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Thesis
{
    public struct HitInfo
    {
        public HitInfo( Entity Attacker = null, Entity Reciever = null, float DamageAmount = 0, HumanBodyBones HitPart = HumanBodyBones.LastBone )
        { this.Attacker = Attacker; this.Reciever = Reciever; this.DamageAmount = DamageAmount; this.HitPart = HitPart; }

        public Entity Attacker;
        public Entity Reciever;

        public float DamageAmount;
        public HumanBodyBones HitPart;
    }

    interface DamageReceiver
    {
        void ReceiveDamage( HitInfo hitinfo );
    }

    class DamageRedirector : Entity, DamageReceiver
    {
        HumanBodyBones BoneType;

        public Body Owner;

        protected virtual void Start()
        {
            Owner = transform.root.GetComponentInChildren<Body>();
            BoneType = GetBoneType( this );
        }

        public virtual void ReceiveDamage( HitInfo hitinfo )
        {
            hitinfo.Reciever = Owner;
            hitinfo.HitPart = BoneType;

            EnvironmentManager.RegistredHits.Add( hitinfo );

            Owner.ReceiveDamage( hitinfo );
        }

        public static HumanBodyBones GetBoneType( DamageRedirector redirector )
        {
            for( HumanBodyBones i = 0; i <= HumanBodyBones.LastBone; i++ )
                if( redirector.Owner.animator.GetBoneTransform( i ) == redirector.transform )
                    return i;

            return HumanBodyBones.LastBone;
        }
    }
}
