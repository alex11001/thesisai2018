﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Thesis
{
    public partial class Body : EntitySided, DamageReceiver
    {
        public float Health = 100;
        public bool IsAlive { get { return Health > 0; } }

        public Weapon EquipedWeapon { get { return Inventory.EquipedWeapon; }  }

        public virtual void AddWeapon( Weapon weapon )
        {
            Inventory.Add( weapon );
        }

        public virtual void AddAndEquipWeapon( Weapon weapon )
        {
            if( weapon == null )
                return;

            AddWeapon( weapon );
            EquipWeapon( weapon );
        }

        public virtual void DropWeapon( Weapon weapon )
        {
            Inventory.Remove( weapon );
        }

        public virtual void EquipWeapon( Weapon weapon )
        {
            //body stuff

            Inventory.Equip( weapon );
        }

        public virtual void HolsterWeapon( Weapon weapon )
        {
            //body stuff

            Inventory.Holster( weapon );
        }

        public virtual void ReloadWeapon()
        {
            if( Inventory.EquipedWeapon == null )
                return;

            Inventory.EquipedWeapon.Reload( AmmoManager.SpawnClip( Inventory.EquipedWeapon.ClipPosition.position, Inventory.EquipedWeapon.ClipPosition.rotation, "DefaultClip" ) ); //Instantiate( Clip.Instance, Inventory.EquipedWeapon.ClipPosition.position, Inventory.EquipedWeapon.ClipPosition.rotation ) );
        }

        public virtual void ReceiveDamage( HitInfo hitinfo )
        {
            float damage_received = hitinfo.DamageAmount * GetBoneModifier( hitinfo.HitPart );
            Health -= damage_received;
            CurrentController.StateInfo.Heat += damage_received;
        }

        protected virtual float GetBoneModifier( HumanBodyBones bone )
        {
            switch( bone )
            {
                case HumanBodyBones.Head:
                    return 10;
                case HumanBodyBones.RightHand:
                case HumanBodyBones.RightFoot:
                case HumanBodyBones.LeftHand:
                case HumanBodyBones.LeftFoot:
                    return 0.25f;
                case HumanBodyBones.RightLowerArm:
                case HumanBodyBones.RightLowerLeg:
                case HumanBodyBones.LeftLowerArm:
                case HumanBodyBones.LeftLowerLeg:
                    return 0.5f;
                case HumanBodyBones.RightUpperArm:
                case HumanBodyBones.RightUpperLeg:
                case HumanBodyBones.LeftUpperArm:
                case HumanBodyBones.LeftUpperLeg:
                case HumanBodyBones.RightShoulder:
                case HumanBodyBones.LeftShoulder:
                    return 0.75f;
                default: return 1;
            }
        }
    }
}