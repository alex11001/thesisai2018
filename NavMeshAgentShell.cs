﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Assets.Thesis.Extends;

namespace Assets.Thesis
{
    public class NavMeshAgentShell
    {
        public float DestinationTolerance = .01f;

        int PathAdditionIndex = 0;
        Vector3[] PathAddition;

        NavMeshQueryFilter CurrentAgentFilter;

        public Transform parent;
        public NavMeshAgent agent;
        public NavMeshAgentShell()
        {
            CurrentAgentFilter = new NavMeshQueryFilter();
        }

        public NavMeshAgentShell( NavMeshAgent agent, Transform parent = null ) : this()
        {
            this.agent = agent;
            this.parent = parent;
            UpdateInfo();

            agent.updateRotation = false;
            agent.updatePosition = false;
            agent.updateUpAxis = false;
            agent.speed = 1;
            agent.enabled = true;
        }

        public void UpdateInfo()
        {
            CurrentAgentFilter.agentTypeID = agent.agentTypeID;
            CurrentAgentFilter.areaMask = agent.areaMask;
        }

        public void MoveTo( Vector3 target )
        {
            agent.SetDestination( target );
            agent.nextPosition = agent.transform.position;
        }

        //public void UpdatePosition()
        //{
        //    agent.nextPosition = parent.position;
        //    UpdatePathAddition();
        //}

        //public void UpdatePosition( Vector3 pos )
        //{
        //    agent.nextPosition = pos;
        //    UpdatePathAddition();
        //}

        //public void SetDestination( Vector3 target )
        //{
        //    agent.SetDestination( target );
        //    PathAddition = null;
        //}

        //public void SetDestination( Vector3 target, Vector3[] pathAddition )
        //{
        //    PathAddition = new Vector3[pathAddition.Length + 2];
        //    PathAddition[0] = agent.transform.position;
        //    pathAddition.CopyTo( PathAddition, 1 );

        //    PathAddition[PathAddition.Length - 1] = target;
        //    PathAdditionIndex = 0;

        //    agent.SetDestination( agent.transform.position );
        //    UpdatePathAddition();
        //}


        //public void UpdatePathAddition()
        //{
        //    if( PathAddition == null )
        //        return;

        //    if( IsReachDestination )
        //    {
        //        PathAdditionIndex++;

        //        if( PathAdditionIndex >= PathAddition.Length )
        //        {
        //            PathAddition = null;
        //            return;
        //        }

        //        agent.SetDestination( PathAddition[PathAdditionIndex] );
        //    }
        //}

        public bool IsReachDestination
        {
            get
            {
                return (agent.remainingDistance < DestinationTolerance);//Vector3Extend.NearlyEquals( agent.transform.position, , 0.1f );
            }
        }


        //public NavMeshPath BuildPathWithPoints( Vector3[] points, bool applyToAgent = false )
        //{
        //    NavMeshPath result_path = new NavMeshPath();
        //    NavMeshPath temp_path = new NavMeshPath();

        //    int count = points.Length - 1;
        //    for( int i = 0; i < count; i++ )
        //    {
        //        NavMesh.CalculatePath( points[i], points[i + 1], CurrentAgentFilter, temp_path );
        //        temp_path.
        //        //  )
        //        //points
        //    }
        //}
    }
}