﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Thesis
{
    public class Order_Patrol : Order
    {
        public Order_Patrol( Vector3[] route = null, Order nextorder = null ) : base(nextorder) { PatrolRoute = route; }

        public Vector3[] PatrolRoute;
    }
}
