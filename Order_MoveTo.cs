﻿using UnityEngine;

namespace Assets.Thesis
{
    public class Order_MoveTo : Order
    {
        public Order_MoveTo( Vector3 point = new Vector3(), Order nextorder = null ) : base(nextorder) { Point = point; }

        public Vector3 Point;

        public override Order NextOrderDefault { get { return new Order_ProtectSelf(); } }
        public override bool IsOrderSucceed( BodyController_AI controller ) { return ((controller.NavAgent.agent.destination == Point) && controller.NavAgent.IsReachDestination); }
    }
}
