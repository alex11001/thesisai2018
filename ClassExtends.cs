﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Thesis.Extends
{
    public static class Extend
    {
        public static bool NearlyEquals( this Vector3 v1, Vector3 v2, float tolerance = 0.01f )
        {
            return (Vector3.Distance( v1, v2 ) <= tolerance);
        }

        public static bool NearlyEquals( this float f1, float f2, float tolerance = 0.01f )
        {
            return Mathf.Abs( Mathf.Abs(f1)-Mathf.Abs(f2)) <= tolerance;
        }

        /// <summary> Sets the local space position and rotation of the Transform component. </summary>
        public static void SetLocalPositionAndRotation<T>( this T t, Vector3 position, Quaternion rotation ) where T : Transform
        {
            t.localPosition = position; t.localRotation = rotation;
        }

        public static void Swap<T>( ref T left, ref T right )
        {
            T temp = left;
            left = right;
            right = temp;
        }
    }
}

namespace Assets.Thesis
{
    [Serializable]
    public class PositionRotation { public Vector3 position; public Vector3 rotation; }
}